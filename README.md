# ITESCAM ISC Electronic Principles and Digital Applications

This are the notes of the 2017-2018P course of Electronic Principles and Digital Applications imparted at ITESCAM in Calkiní, Campeche.

The idea behind this repository is the need for have the materials of the course available for everyone who needs it, even myself, and have a version control for future modifications and adjustments.

This course is made in Spanish.

## Description

In this repository, you can find three main folders: "Clases/", "Prácticas/" and "Tareas/":

* Clases: This folder contains the main PDF notes of the course, and the LaTex source files for modifications and compilation.

* Prácticas: This folder contains a proposal of practices to be made by the students. They have an order to be made, thinking on the evolution of competences of the student. Inside every practice, there are a description and a set of steps to build a technical report, a set of suggestions, and some rules and safety tips.

* Tareas: Like the practices folder, it contains a proposal of homework to be made by students along with the course.

Every material given here are adjusted to the subject profile made by the Tecnológico Nacional de México, shown in **SCD-1018.PDF** file.

## Acknowledgment

I want to thank to **Instituto Tecnológico de Calkiní en el Estado de Campeche - ITESCAM** for the opportunity to give this awesome course, and also, I want to thank to **Dr. José Luis Lira Turriza**, career coordinator, for the invitation and support in the elaboration of this project.
